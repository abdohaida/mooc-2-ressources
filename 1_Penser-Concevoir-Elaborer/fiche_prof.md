**Thématique :** Algorithme et programmation.

**Notions liées :** notion d'algorithme et structure de base .

**Résumé de l’activité :** 3 Activités débranchées simple pour comprendre les notion d'algorithme et variable.

**Objectifs :** Découverte la notion d'algorithme : structure de base ,notion de variable.

**Auteur :** Haida Abdessalam

**Durée de l’activité :** 2h 

**Forme de participation :** individuelle .

**Matériel nécessaire :** Papier, stylo !

**Préparation :** Aucune

**Autres références :** 

**Fiche élève cours :** Disponible [ici](https://gitlab.com/abdohaida/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/notion-d-algorithme-et-instructions-de-base.pdf)

**Fiche élève activité :** Disponible [ici](https://gitlab.com/abdohaida/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/activité_élève.pdf)   