# Mise en oeuvre d'une seance

**Fiche élève cours :** Disponible [ici](https://gitlab.com/abdohaida/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/notion-d-algorithme-et-instructions-de-base.pdf)

**Fiche élève activité :** Disponible [ici](https://gitlab.com/abdohaida/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/activité_élève.pdf) 

**Fiche prof :** Disponible [ici](https://gitlab.com/abdohaida/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof.md)

**Etape 1 : partie 1 (notion d'algorithme)** 
dans cette etape chaque apprenant doit essayer d'organiser dans le bon order les étiquettes de pour nous donner un algorithme a suivre par omar se préparer.
	
l'enseignant va aider les apprenants dans ce travail:
- plusieurs possibilités pour résoudre ce problème.
- l'ordre des étiquettes il faut qu'il soit logique.
- chaque apprenent présentera son travail.
apés on va choisi le meilleur algorithme comme solution final de problème.

**Etape 2 : partie 2 (avec les nombres)** 
le travail se fait de façan individuelle , les apprenants vont décrit les etapes à suivre pour effectuer un calcul.

l'enseignant donne les consignes respecté et orienter les apprenants vers le bon endroit:
- une seul possibilité (pour nous donne la même resultat).
- fait appele aux règles de mathématiques (priotite et associativité a gauche)

à la fin de cette partie partie vont decouvrire la notion d'algorithme et l'importance de l'ordre des instructions.

**Etape 2 : partie 3 (notion de variable)** 
dans cette partie le travail en binôme avec un brainstorming sur la signification des images (verre d'eau, verre de thé, verre de café, verre de jus) pour faire un analogie concenant les variables (nom, valeur, type)

- le contenue de chaque verre représente une valeur (changeable) pour les variables.
- ce contenue il a un type (eau, jus, thé, café).
- par exemple lorsqu'on a beaucoup de verre d'eaux on les distinguer par les noms (verre de ahmed, verre de khalid, ...)
